SET @sql = NULL;

SELECT GROUP_CONCAT(DISTINCT
           CONCAT(
             'max(CASE WHEN ma.date_meeting = ''',
             date_format(date_meeting, '%Y-%m-%d'),
             ''' THEN ''P'' ELSE ''A'' END) AS `',
             date_format(date_meeting, '%Y-%m-%d'), '`'
               )
           ) INTO @sql
FROM va_meeting
WHERE va_meeting.date_meeting >= '2021-08-01'
AND va_meeting.date_meeting <= '2021-08-30';

SET @sql
= CONCAT('SELECT CONCAT(ma.last_name,'' '',ma.first_name) as full_name, ', @sql, '
            from
            (
              select meeting.date_meeting, person.last_name, person.first_name
              from va_meeting as meeting
              join va_attendance va1 on va1.meeting_id = meeting.id
              join va_student student on student.id = va1.student_id
              join va_person person on person.id = student.person_id
            ) ma
            where
            ma.date_meeting >= ''2021-08-01''
            AND ma.date_meeting <= ''2021-08-30''
            GROUP BY full_name
            ORDER BY full_name
            ');

PREPARE stmt FROM @sql;
EXECUTE stmt;