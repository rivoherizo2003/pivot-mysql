-- Adminer 4.8.1 MySQL 8.0.25 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `va_attendance`;
CREATE TABLE `va_attendance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `meeting_id` int NOT NULL,
  `update_user_id` int DEFAULT NULL,
  `added_user_id` int DEFAULT NULL,
  `arrival_time` time DEFAULT NULL,
  `addedDate` datetime DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C3BD89B99BF1FC2A` (`student_id`),
  KEY `IDX_C3BD89B967433D9C` (`meeting_id`),
  KEY `IDX_C3BD89B9E0DFCA6C` (`update_user_id`),
  KEY `IDX_C3BD89B9970D0589` (`added_user_id`),
  CONSTRAINT `FK_C3BD89B967433D9C` FOREIGN KEY (`meeting_id`) REFERENCES `va_meeting` (`id`),
  CONSTRAINT `FK_C3BD89B9970D0589` FOREIGN KEY (`added_user_id`) REFERENCES `va_users` (`id`),
  CONSTRAINT `FK_C3BD89B99BF1FC2A` FOREIGN KEY (`student_id`) REFERENCES `va_student` (`id`),
  CONSTRAINT `FK_C3BD89B9E0DFCA6C` FOREIGN KEY (`update_user_id`) REFERENCES `va_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `va_attendance` (`id`, `student_id`, `meeting_id`, `update_user_id`, `added_user_id`, `arrival_time`, `addedDate`, `updatedDate`) VALUES
(1,	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL),
(4,	1,	4,	NULL,	NULL,	NULL,	NULL,	NULL),
(5,	1,	6,	NULL,	NULL,	NULL,	NULL,	NULL),
(6,	2,	6,	NULL,	NULL,	NULL,	NULL,	NULL),
(7,	2,	5,	NULL,	NULL,	NULL,	NULL,	NULL),
(8,	3,	6,	NULL,	NULL,	NULL,	NULL,	NULL),
(9,	3,	1,	NULL,	NULL,	NULL,	NULL,	NULL),
(10,	3,	5,	NULL,	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `va_meeting`;
CREATE TABLE `va_meeting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `update_user_id` int DEFAULT NULL,
  `added_user_id` int DEFAULT NULL,
  `date_meeting` datetime NOT NULL,
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_start` time NOT NULL,
  `addedDate` datetime DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` smallint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B7BAD763E0DFCA6C` (`update_user_id`),
  KEY `IDX_B7BAD763970D0589` (`added_user_id`),
  CONSTRAINT `FK_B7BAD763970D0589` FOREIGN KEY (`added_user_id`) REFERENCES `va_users` (`id`),
  CONSTRAINT `FK_B7BAD763E0DFCA6C` FOREIGN KEY (`update_user_id`) REFERENCES `va_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `va_meeting` (`id`, `update_user_id`, `added_user_id`, `date_meeting`, `title`, `description`, `time_start`, `addedDate`, `updatedDate`, `address`, `active`) VALUES
(1,	NULL,	NULL,	'2021-08-04 00:00:00',	'meeting',	'meeting 1',	'18:00:00',	NULL,	NULL,	'',	1),
(4,	NULL,	NULL,	'2021-08-05 00:00:00',	'meeting',	'meeting 2',	'18:00:00',	NULL,	NULL,	'',	1),
(5,	NULL,	NULL,	'2021-08-06 00:00:00',	'meeting',	'meeting 3',	'18:00:00',	NULL,	NULL,	'',	1),
(6,	NULL,	NULL,	'2021-08-07 00:00:00',	'meeting',	'meeting 4',	'18:00:00',	NULL,	NULL,	'',	1);

DROP TABLE IF EXISTS `va_person`;
CREATE TABLE `va_person` (
  `id` int NOT NULL AUTO_INCREMENT,
  `update_user_id` int DEFAULT NULL,
  `added_user_id` int DEFAULT NULL,
  `first_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2972E75FE0DFCA6C` (`update_user_id`),
  KEY `IDX_2972E75F970D0589` (`added_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `va_person` (`id`, `update_user_id`, `added_user_id`, `first_name`, `last_name`) VALUES
(1,	NULL,	NULL,	'Rakoto',	'Noel'),
(2,	NULL,	NULL,	'Jack',	'Sparrow'),
(3,	NULL,	NULL,	'Robert',	'de Niro');

DROP TABLE IF EXISTS `va_student`;
CREATE TABLE `va_student` (
  `id` int NOT NULL AUTO_INCREMENT,
  `person_id` int NOT NULL,
  `update_user_id` int DEFAULT NULL,
  `added_user_id` int DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `addedDate` datetime DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2BC7F8E5217BBB47` (`person_id`),
  KEY `IDX_2BC7F8E5E0DFCA6C` (`update_user_id`),
  KEY `IDX_2BC7F8E5970D0589` (`added_user_id`),
  CONSTRAINT `FK_2BC7F8E5217BBB47` FOREIGN KEY (`person_id`) REFERENCES `va_person` (`id`),
  CONSTRAINT `FK_2BC7F8E5970D0589` FOREIGN KEY (`added_user_id`) REFERENCES `va_users` (`id`),
  CONSTRAINT `FK_2BC7F8E5E0DFCA6C` FOREIGN KEY (`update_user_id`) REFERENCES `va_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `va_student` (`id`, `person_id`, `update_user_id`, `added_user_id`, `is_active`, `addedDate`, `updatedDate`) VALUES
(1,	1,	NULL,	NULL,	1,	NULL,	NULL),
(2,	2,	NULL,	NULL,	1,	NULL,	NULL),
(3,	3,	NULL,	NULL,	1,	NULL,	NULL);

-- 2021-08-05 14:07:30