# pivot-mysql

This repository is an example to simulate PIVOT which is not supported in mysql

# Create the database
- Create a database in your phpmyadmin or adminer;

# Import data and table structure
- Import the file data_structure.sql in your database;

# Launch the file pivot script
- Launch the file pivot.sql

That's all folks!
